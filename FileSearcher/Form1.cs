﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace FileSearcher
{
    public partial class SearchFile : Form
    {
        /// <summary>
        /// Класс для сериализации введенных критериев поиска
        /// </summary>

        [Serializable]
        public class SearchParam
        {
            public string FileName { get; set; }
            public string TextCut { get; set; }
        }


        private bool isPaused, inThread;
        private Stopwatch Timer;

        public SearchFile()
        {
            /// <summary>
            /// Загрузка данных и установка событий по сериализации
            /// </summary>

            InitializeComponent();
            LoadLabelData();

            filenameTBox.TextChanged += new EventHandler((Object o, EventArgs a) =>
            {
                SaveLabelData();
            });

            filetextTBox.TextChanged += new EventHandler((Object o, EventArgs a) =>
            {
                SaveLabelData();
            });

        }

        private async void startSearchButton_Click(object sender, EventArgs e)
        {

            /// <summary>
            /// При запуске устанавливаем неактивность кнопок и возвращаем начальные значения
            /// </summary>

            uint totalFiles = 0;
            foundListBox.Items.Clear();
            stopSearchButton.Enabled = pauseThread.Enabled = inThread = true;
            startSearchButton.Enabled = isPaused = false;
            Timer = new Stopwatch();
            searchtreeView.Nodes.Clear();
            string path = string.Empty;

            /// <summary>
            /// Диалог для выбора директории 
            /// </summary>

            using (FolderBrowserDialog dlg = new FolderBrowserDialog())
            {
                if (dlg.ShowDialog() == DialogResult.OK)
                    path = dlg.SelectedPath;
                else
                {
                    startSearchButton.Enabled = true;
                    inThread = isPaused = unpauseThread.Enabled =
                        pauseThread.Enabled = stopSearchButton.Enabled = false;
                    return;
                }
            }

            /// <summary>
            /// Начало обработки файлов 
            /// </summary>

            Timer.Start();
            var stack = new Stack<TreeNode>();
            var rootDirectory = new DirectoryInfo(path);
            var node = new TreeNode(rootDirectory.Name) { Tag = rootDirectory };
            stack.Push(node);

            while (stack.Count > 0)
            {
                var currentNode = stack.Pop();
                var directoryInfo = (DirectoryInfo)currentNode.Tag;
                foreach (var directory in directoryInfo.GetDirectories())
                {
                    var childDirectoryNode = new TreeNode(directory.Name) { Tag = directory };
                    currentNode.Nodes.Add(childDirectoryNode);
                    stack.Push(childDirectoryNode);
                }
                foreach (var file in directoryInfo.GetFiles())
                {
                    await Task.Delay(1);

                    /// <summary>
                    /// Симуляция паузы
                    /// </summary>

                    await Task.Run(() =>
                    {
                        while (isPaused) Task.Delay(1);
                    });

                    if (!inThread)
                    {
                        return;
                    }

                    /// <summary>
                    /// Запись найденных файлов в журнал (foundListBox)
                    /// </summary>
                    await Task.Run(() =>
                    {
                        if (((File.ReadAllText(file.DirectoryName + '\\' + file.Name).Contains(filetextTBox.Text)
                        && filetextTBox.Text.Length > 0) ||
                              (file.Name == filenameTBox.Text)))
                        {
                            Invoke(new Action(() =>
                            {
                                foundListBox.Items.Add(file.DirectoryName + file.Name);
                                foundListBox.SelectedIndex = foundListBox.Items.Count - 1;
                            }));
                        }

                    });

                    /// <summary>
                    /// Отображение пройденного времени, обрабатываемого файла, обработанных всего файлов
                    /// Обработка "списка как в проводнике"
                    /// </summary>

                    elapsedTime.Text = string.Format("{0:mm\\:ss}", Timer.Elapsed);
                    currentFileName.Text = file.Name;
                    fileAmount.Text = (++totalFiles).ToString();
                    currentNode.Nodes.Add(new TreeNode(file.Name));
                    searchtreeView.Nodes.Clear();
                    searchtreeView.Nodes.Add(node);
                    searchtreeView.ExpandAll();
                }
            }
            Timer.Stop();
        }


        /// <summary>
        /// Пауза таймера, деактивация и активация нужных кнопок
        /// </summary>

        private void pauseThread_Click(object sender, EventArgs e)
        {
            Timer.Stop();
            unpauseThread.Enabled = isPaused = true;
            pauseThread.Enabled = false;
        }

        /// <summary>
        /// Возобновение таймера, деактивация и активация нужных кнопок
        /// </summary>

        private void unpauseThread_Click(object sender, EventArgs e)
        {
            Timer.Start();
            pauseThread.Enabled = true;
            isPaused = unpauseThread.Enabled = false;
        }


        /// <summary>
        /// Сброс и отключение таймера, деактивация и активация нужных кнопок
        /// </summary>

        private void stopSearchButton_Click(object sender, EventArgs e)
        {

            Timer.Reset();
            startSearchButton.Enabled = true;
            inThread = isPaused = unpauseThread.Enabled =
                pauseThread.Enabled = stopSearchButton.Enabled = false;
        }


        /// <summary>
        /// Сохранение введенных критериев
        /// </summary>

        private void SaveLabelData()
        {
            string settingsFile = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), "Search_Parameters.txt");
            SearchParam settings = new SearchParam();
            settings.FileName = filenameTBox.Text;
            settings.TextCut = filetextTBox.Text;
            SaveLoadData.Save<SearchParam>(settings, settingsFile);

        }

        /// <summary>
        /// Загрузка введенных ранее критериев
        /// </summary>

        private void LoadLabelData()
        {
            string settingsFile = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), "Search_Parameters.txt");
            SearchParam settings = new SearchParam();
            SearchParam loadedSettings = SaveLoadData.Load<SearchParam>(settingsFile);
            try
            {
                filenameTBox.Text = (loadedSettings.FileName != null) ? loadedSettings.FileName : "";
                filetextTBox.Text = (loadedSettings.TextCut != null) ? loadedSettings.TextCut : "";
            }
            catch (Exception)
            {
                filenameTBox.Text = filetextTBox.Text = "";
            }

        }
    }
}