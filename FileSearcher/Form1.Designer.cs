﻿namespace FileSearcher
{
    partial class SearchFile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.searchtreeView = new System.Windows.Forms.TreeView();
            this.filenameTBox = new System.Windows.Forms.TextBox();
            this.filetextTBox = new System.Windows.Forms.TextBox();
            this.startSearchButton = new System.Windows.Forms.Button();
            this.pauseThread = new System.Windows.Forms.Button();
            this.unpauseThread = new System.Windows.Forms.Button();
            this.stopSearchButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.currentFileName = new System.Windows.Forms.Label();
            this.fileAmount = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.elapsedTime = new System.Windows.Forms.Label();
            this.foundListBox = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // searchtreeView
            // 
            this.searchtreeView.Location = new System.Drawing.Point(12, 12);
            this.searchtreeView.Name = "searchtreeView";
            this.searchtreeView.Size = new System.Drawing.Size(428, 425);
            this.searchtreeView.TabIndex = 0;
            // 
            // filenameTBox
            // 
            this.filenameTBox.Location = new System.Drawing.Point(471, 42);
            this.filenameTBox.Name = "filenameTBox";
            this.filenameTBox.Size = new System.Drawing.Size(100, 20);
            this.filenameTBox.TabIndex = 2;
            // 
            // filetextTBox
            // 
            this.filetextTBox.Location = new System.Drawing.Point(471, 68);
            this.filetextTBox.Name = "filetextTBox";
            this.filetextTBox.Size = new System.Drawing.Size(100, 20);
            this.filetextTBox.TabIndex = 3;
            // 
            // startSearchButton
            // 
            this.startSearchButton.Location = new System.Drawing.Point(446, 412);
            this.startSearchButton.Name = "startSearchButton";
            this.startSearchButton.Size = new System.Drawing.Size(75, 25);
            this.startSearchButton.TabIndex = 4;
            this.startSearchButton.Text = "StartSearch";
            this.startSearchButton.UseVisualStyleBackColor = true;
            this.startSearchButton.Click += new System.EventHandler(this.startSearchButton_Click);
            // 
            // pauseThread
            // 
            this.pauseThread.Enabled = false;
            this.pauseThread.Location = new System.Drawing.Point(527, 381);
            this.pauseThread.Name = "pauseThread";
            this.pauseThread.Size = new System.Drawing.Size(75, 25);
            this.pauseThread.TabIndex = 5;
            this.pauseThread.Text = "Pause";
            this.pauseThread.UseVisualStyleBackColor = true;
            this.pauseThread.Click += new System.EventHandler(this.pauseThread_Click);
            // 
            // unpauseThread
            // 
            this.unpauseThread.Enabled = false;
            this.unpauseThread.Location = new System.Drawing.Point(527, 412);
            this.unpauseThread.Name = "unpauseThread";
            this.unpauseThread.Size = new System.Drawing.Size(75, 25);
            this.unpauseThread.TabIndex = 6;
            this.unpauseThread.Text = "Unpause";
            this.unpauseThread.UseVisualStyleBackColor = true;
            this.unpauseThread.Click += new System.EventHandler(this.unpauseThread_Click);
            // 
            // stopSearchButton
            // 
            this.stopSearchButton.Enabled = false;
            this.stopSearchButton.Location = new System.Drawing.Point(446, 381);
            this.stopSearchButton.Name = "stopSearchButton";
            this.stopSearchButton.Size = new System.Drawing.Size(75, 25);
            this.stopSearchButton.TabIndex = 7;
            this.stopSearchButton.Text = "StopSearch";
            this.stopSearchButton.UseVisualStyleBackColor = true;
            this.stopSearchButton.Click += new System.EventHandler(this.stopSearchButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(446, 142);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Обрабатываемый файл:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(446, 165);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(139, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Количество обр-х файлов:";
            // 
            // currentFileName
            // 
            this.currentFileName.AutoSize = true;
            this.currentFileName.Location = new System.Drawing.Point(590, 142);
            this.currentFileName.Name = "currentFileName";
            this.currentFileName.Size = new System.Drawing.Size(12, 13);
            this.currentFileName.TabIndex = 10;
            this.currentFileName.Text = "\\";
            // 
            // fileAmount
            // 
            this.fileAmount.AutoSize = true;
            this.fileAmount.Location = new System.Drawing.Point(591, 165);
            this.fileAmount.Name = "fileAmount";
            this.fileAmount.Size = new System.Drawing.Size(12, 13);
            this.fileAmount.TabIndex = 11;
            this.fileAmount.Text = "\\";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(446, 188);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Времени прошло:";
            // 
            // elapsedTime
            // 
            this.elapsedTime.AutoSize = true;
            this.elapsedTime.Location = new System.Drawing.Point(591, 188);
            this.elapsedTime.Name = "elapsedTime";
            this.elapsedTime.Size = new System.Drawing.Size(12, 13);
            this.elapsedTime.TabIndex = 13;
            this.elapsedTime.Text = "\\";
            // 
            // foundListBox
            // 
            this.foundListBox.FormattingEnabled = true;
            this.foundListBox.Location = new System.Drawing.Point(451, 215);
            this.foundListBox.Name = "foundListBox";
            this.foundListBox.Size = new System.Drawing.Size(238, 160);
            this.foundListBox.TabIndex = 14;
            // 
            // SearchFile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(701, 449);
            this.Controls.Add(this.foundListBox);
            this.Controls.Add(this.elapsedTime);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.fileAmount);
            this.Controls.Add(this.currentFileName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.stopSearchButton);
            this.Controls.Add(this.unpauseThread);
            this.Controls.Add(this.pauseThread);
            this.Controls.Add(this.startSearchButton);
            this.Controls.Add(this.filetextTBox);
            this.Controls.Add(this.filenameTBox);
            this.Controls.Add(this.searchtreeView);
            this.Name = "SearchFile";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView searchtreeView;
        private System.Windows.Forms.TextBox filenameTBox;
        private System.Windows.Forms.TextBox filetextTBox;
        private System.Windows.Forms.Button startSearchButton;
        private System.Windows.Forms.Button pauseThread;
        private System.Windows.Forms.Button unpauseThread;
        private System.Windows.Forms.Button stopSearchButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label currentFileName;
        private System.Windows.Forms.Label fileAmount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label elapsedTime;
        private System.Windows.Forms.ListBox foundListBox;
    }
}

