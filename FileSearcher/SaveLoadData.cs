﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace FileSearcher
{
    class SaveLoadData
    {
        /// <summary>
        /// Сохранить экземпляр класса настроек приложения
        /// </summary>
        /// <typeparam name="T">Тип сохраняемых данных</typeparam>
        /// <param name="settings">Экземпляр класса для сохранения</param>
        /// <param name="FileName">Путь к файлу, в который будут сохранены настройки</param>
        public static void Save<T>(T settings, string FileName)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            using (FileStream fs = new FileStream(FileName, FileMode.OpenOrCreate))
            {
                using (StreamWriter streamWriter = new StreamWriter(fs))
                {
                    xmlSerializer.Serialize(streamWriter, settings);
                }
            }
        }

        /// <summary>
        /// Загрузить настройки из файла
        /// </summary>
        /// <typeparam name="T">Тип сохраняемых данных</typeparam>
        /// <param name="FileName">Путь к файлу</param>
        /// <returns>Экземпляр класса настроек</returns>
        public static T Load<T>(string FileName)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));

            try
            {
                using (FileStream fs = new FileStream(FileName, FileMode.Open))
                {
                    using (StreamReader streamReader = new StreamReader(fs))
                    {
                        return (T)xmlSerializer.Deserialize(streamReader);
                    }
                }
            }
            catch (Exception)
            {
                if (File.Exists(FileName))
                {
                    File.Delete(FileName);
                }
                return default(T);
            }
        }
    }
}